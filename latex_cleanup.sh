#!/bin/bash

find . -name "*.aux" -type f -delete
find . -name "*.auxlock" -type f -delete
find . -name "*.fdb_latexmk" -type f -delete
find . -name "*.fls" -type f -delete
find . -name "*.log" -type f -delete
find . -name "*.out" -type f -delete
find . -name "*.synctex.gz" -type f -delete
find . -name "*.swp" -type f -delete
find . -name "*.toc" -type f -delete
find . -name "*.md5" -type f -delete
find . -name "*.dpth" -type f -delete
