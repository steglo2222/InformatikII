\section{Dynamische Programmierung}
Ergebnisse von Teilproblemen werden tabelliert. 
Dazu dürfen die Teilprobleme keine zirkulären
Abhängigkeit besitzen. Der Teilproblem-Graph darf
keine Zyklen enthalten.
\subsubsection{Generelles Vorgehen}
\begin{enumerate}
	\item DP-Tabelle mit Resultaten von Teilproblemen erstellen.\\
		$\rightarrow$ Dimension der DP-Tabelle?
	\item Berechnung von Randfällen, also Einträgen in der DP-Tabelle, 
		die nicht von anderen abhängen.
	\item Berechnungsreihenfolge bestimmen.
	\item Auslesen der Lösung.
\end{enumerate}
\begin{complexity}{Dynamic Programming}
Die typische Laufzeit entspricht der Anzahl Einträge mal Aufwand pro Eintrag.
\end{complexity}

\subsection{Unterschied zu Divide-and-Conquer}
In beiden Fällen wird das Problem in Teilprobleme unterteilt. 
Im Unterschied zu klassischen Divide-and-Conquer-Algorithmen (z.B. Mergesort)
sind Teilprobleme \textbf{nicht unabhängig}. Bei DP gibt es \textbf{überlappende 
Teilprobleme,} die im Algorithmus mehrfach gebraucht werden.

\subsection{Beispiele}
\subsubsection{Stäbe schneiden}
Verschieden Möglichkeiten, einen Stab der Länge 4 zu zerschneiden (ohne Permutationen):
\begin{center}
\input{figures/rods.tex}
\end{center}
Verschiedene Längen haben unterschiedliche Preise. Problem: Finde beste Kombination, um
Gesamtwert zu maximieren.
\begin{algorithm}[H]
	\KwData{Länge $n\geq 0$, Preise $v_i$, Tabelle $m$}
	\KwResult{bester Wert $q$}
	$q=0$\;
	\If{$n>0$}{
		\eIf{$\exists m[n]$}{
			$q=m[n]$\;
		}{
			\For{$i=1$ \KwTo $n$}{
				$q=$ max\{$q$,$v_i+$ RodCutMemoized($m,v,n-i)$\}\;
			}
			$m[n]=q$\;
		}
	}
	\Return q\;
	\caption{RodCutMemoized($m,v,n$)}
\end{algorithm}
\begin{complexity}{RodCutMemoized}
	$\sum_{i=1}^n i = \Theta(n^2)$
\end{complexity}
\subsubsection{Kaninchen}
Gegeben: $n \times n$-Gitter, Startpunkt $(1,1)$ mit 
bestimmter Anzahl Rüben auf den Kanten. \\
$\rightarrow$ Gesamtanzahl Rüben 
auf dem Weg $(1,1) \rightarrow (n,n)$ maximieren.
$n\times n$-DP-Tabelle $T_{ij}$ erstellen: Eintrag bei $(i,j)$ enthält
die maximale Anzahl Rüben von $(i,j)$ nach $(n, n)$.
\begin{align*}
	&T_{ij} =\\
	&\begin{cases}
		\textrm{max}\{w_{(i,j)-(i,j+1)}+T_{i,j+1},w_{(i,j)-(i+1,j)}+T_{i+1,j}\}, & i<n, j<n\\
		w_{(i,j)-(i,j+1)}+T_{i,j+1}, & i=n, j<n\\
		w_{(i,j)-(i+1,j)+T_{i+1,j}}, & i<n, j=n\\
		0 & i=j=n
	\end{cases}\\
\end{align*}
\subsubsection{Längste aufsteigende Teilfolge}
Gegeben sei die Folge $(a_i)_i = (13,12,15,11,16,14)$, in 
der man die längste aufsteigende Teilfolge (LAT) sucht.
Teilfolgen werden gesucht, indem Elemente dem Index $i$ nach 
in untenstehende Tabelle eingefügt werden. Der Slot $j$ 
beinhaltet dabei jeweils die \glqq{}beste\grqq{} (tieferer Wert = besser) Teilfolge der 
Länge $j$. 
\input{figures/dp_lat.tex}
Es ist nur nötig, das jeweils letzte Element 
der Teilfolgen der einzelnen Slots in der DP-Tabelle zu speichern.
Die zu obenstehenden Beispiel gehörende DP-Tabelle $(L_j)_j$ sieht wie folgt aus:
\begin{center}
\input{figures/dp_table_lat.tex}
\end{center}
Um nun die gesuchte Teilfolge aus der DP-Tabelle zu rekonstruieren,
wird eine zweite Tabelle erstellt, die jeweils den Vorgänger 
speichert. Ausgehend vom letzten Eintrag (16) kann so die Teilfolge 
\glqq{}rückverfolgt\grqq{} werden.
\input{figures/vorgaenger_lat.tex}
Somit ist (12, 15, 16) die gesuchte längste aufsteigende Teilfolge.
Zur Implementierung braucht man also zwei Tabellen 
$T[0\dots n]$ und $V[1\dots n]$ die wie oben initialisiert sind. 
Für jeden Neueintrag $a_k$ binäre Suche nach $l$, sodass
$T[l] < a_k < T[l+1]$. Setze $T[l+1]=a_k$ und $V[k]=T[l]$.
\begin{complexity}{LAT}
	Das Finden des Einfügeorts in die DP-Tabelle ist mit $\mathcal{O}(\log n)$ 
	möglich (binäre Suche). Das Traversieren durch die Liste der Vorgänger
	hat Kosten $\mathcal{O}(n)$. Somit ergibt sich ein Gesamtaufwand von $\Theta(n\log n)$.
\end{complexity}
\subsubsection{Minimale Editierdistanz}
Minimale Anzahl von Einfüge-, Lösch- und Ersetzoperationen zwischen 
zwei Zeichenketten, um von $A_n$ zu $B_m$ zu gelangen. 
Bei Levenshtein-Distanz haben alle Operation Aufwand 1.
\begin{itemize}
	\item DP-Tabelle $E$ der Grösse $n \times m$.
	\item Teilprobleme: Editierdistanz $E(i,j)$ von $a_{1\dots i} \rightarrow b_{1\dots j}$.
	\item Einträge berechnen:\\
		Randfälle: $E(0,i)=i$, $E(j,0)=j$. 
		\begin{align*}
			E(i,j)=\min\{&\mathrm{del}(a_i)+E(i-1,j),\\
									 &\mathrm{ins}(b_j)+E(i,j-1),\\
									 &\mathrm{repl}(a_i,b_j)+E(i-1,j-1)\}
		\end{align*}
	\item Gesuchte Lösung ist $E(n,m)$.
\end{itemize}
\begin{complexity}{Editierdistanz}
	Anzahl Tabelleneinträge $(m+1)(n+1)$, Berechnung $\mathcal{O}(mn)$,
	Bestimmung der Lösung $\mathcal{O}(mn)$. Gesamtaufwand also $\mathcal{O}(mn)$.
\end{complexity}
\subsubsection{Bellman-Ford}
Findet kürzester Pfad, auch mit negativen Kantengewichten.
\begin{algorithm}[H]
	\KwData{Graph $G=(V,E,c)$, Startpunkt $s\in V$}
	\KwResult{Wenn Rückgabe true, minimale Gewichte $d$ 
	der kürzesten Pfade zu jedem Knoten, sonst kein 
	kürzester Pfad.}
	\ForEach{$u\in V$}{
		$d_s[u]=\infty$\;
	}
	$d_s[s]=0$\;
	\For{$i=1$ \KwTo $|V|$}{
		$f=$ false\;
		\ForEach{$(u,v)\in E$}{
			$f=f$ || Relax($u,v$)\;
		}
		\If{f = false}{
			\Return true\;
		}
	}
	\Return false\;
	\caption{Bellman-Ford}
\end{algorithm}
\subsubsection{Optimale Suchbäume}
Die Zugriffswahrscheinlichkeiten $p_i$ auf Elemente 
des binären Suchbaums sind a priori 
bekannt. Problem aufteilen in Teilsuchbäume mit 
Knoten $k_1\dots k_{r-1}$ und $k_{r+1}\dots k_n$ die durch 
die Wurzel $k_r$ verbunden sind $\rightarrow$ Wie wählt man $r$?
Seien $E(i,j)$ die Kosten des optimalen Suchbaums $k_i\dots k_j$.
\begin{equation*}
	E(i,j)=\begin{cases}
		0, \textrm{ falls } i>j&\\
		p_i, \textrm{ falls } i=j&\\
		p(i,j)+\min\{E(i,k-1)+E(k+1,j),i\leq k \leq j\} &
	\end{cases}
\end{equation*}
wobei
\begin{equation*}
	p(i,j):=p_i+p_{i+1}+\dots +p_j,\;\; i\leq j
\end{equation*}
Ausgehend von der Hauptdiagonalen die Nebendiagonalen berechnen.
Lösung steht in $E(1,n)$. Separate Tabelle, in der die Argmins gespeichert 
werden.
\begin{complexity}{Optimale Suchbäume}
	Laufzeit $\Theta(n^3)$, Speicherplatz $\Theta(n^2)$.
\end{complexity}
\subsection{Greedy-Algorithmen}
Ein rekursiv lösbares Optimierungsproblem kann \glqq{}gierig\grqq{}
gelöst werden, wenn es folgende Eigenschaften erfüllt:
\begin{itemize}
	\item \textbf{Optimale Suchstruktur: }Lösung eines Problem
		ergibt sich durch Kombination optimaler Teillösungen.
	\item \textbf{greedy choice property: }Die Lösung kann konstruiert 
		werden, indem ein lokales Kriterium herangezogen wird, welches nicht von
		der Lösung der Teilprobleme abhängt. 
\end{itemize}
\subsubsection{Huffman Encoding}
Codewörter variabler Länge, je nach 
Auftrittshäufigkeit. 
\begin{algorithm}[H]
	\KwData{Codewörter $c\in C$}
	\KwResult{Wurzel eines optimalen Codebaums}
	$n=|C|$\;
	$Q=C$\;
	\For{$i=1$ \KwTo $n-1$}{
		Alloziere neuen Knoten $z$\;
		$z$.left = ExtractMin($Q$)\;
		$z$.right = ExtractMin($Q$)\;
		$z$.freq = $z$.left.freq + $z$.right.freq\;
		$Q$.insert($z$)\;
	}
	\Return ExtractMin($Q$)\;
	\caption{Huffman}
\end{algorithm}
Für $Q$ einen Min-Heap verwenden!
\begin{complexity}{Huffman}
	Heap bauen in $\mathcal{O}(n)$, ExtractMin in $\mathcal{O}(\log n)$.
	Somit Laufzeit $\mathcal{O}(n\log n)$.
\end{complexity}



