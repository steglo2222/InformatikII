# Informatik II Zusammenfassung
## ETH D-ITET 3. Semester 252-0836-00L
### General design rules
- Summary may not exceed 8 pages
- Keep everything tidy: Put sections and images in separate files.
- Put global settings or additional packages in fs.sty
- Images: if possible use TikZ, otherwise use license-compatible vector graphics, e.g. from Wikipedia
- Respect color scheme \primaryheader, \secondaryheader
- Use "definition" environment for something that is taken "as is", aka a definition (duh)
- Use "complexity" environment if complexity for an algorithm is known. If possible, separate worst, best and average case
- Add references, sources; respect copyright
